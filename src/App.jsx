import React, { Component } from "react";
import NavBar from "./components/navBar";
import Login from "./components/login";
import Main from "./components/main";
import { Routes, Route, useParams, Outlet, Navigate } from "react-router-dom";

import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons
import "./css/app.css";
import ManageRequestedItem from "./components/manageRequestedItem";

class App extends Component {
  isLoggedIn = () => {
    return localStorage.getItem("user") != null;
  };

  render() {
    const IfLoggedIn = ({ isLoggedIn, redirectTo }) => {
      return isLoggedIn ? redirectTo : <Navigate to="/login" replace />;
    };

    const RequestedItemWrapper = (props) => {
      const params = useParams();
      return <ManageRequestedItem itemId={props.id} />;
    };

    return (
      <>
        <div className="container-fluid">
          <div className="row">
            <NavBar />
          </div>

          <div className="row">
            <Routes>
              <Route
                path="/"
                element={
                  <IfLoggedIn
                    isLoggedIn={this.isLoggedIn()}
                    redirectTo={<Main />}
                  />
                }
              />

              <Route path="/login" element={<Login />} />

              <Route
                path="/itemRequested/:id"
                element={<ManageRequestedItem />}
              />
            </Routes>
          </div>
        </div>
      </>
    );
  }
}

export default App;
