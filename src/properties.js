export const properties = {
  // delivery states
  deliverystate_vor_ort: "Vor Ort",
  deliverystate_angefordert: "Angefordert",
  deliverystate_abholbereit: "Abholbereit",
  deliverystate_abgeholt: "Abgeholt",
  deliverystate_angefordert_aufschub: "Angefordert (Aufschub)",
  deliverystatesActive: [
    "Vor Ort",
    "Angefordert",
    "Angefordert (Aufschub)",
    "Abholbereit",
  ],

  //user roles
  role_industry: "INDUSTRIE",
  role_admin: "ADMIN",
  role_kunde: "KUNDE",

  //urls
  base_url: "https://hdz-backend.herokuapp.com",
  //base_url: "http://localhost:8080",
};
