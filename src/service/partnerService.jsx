import axios from "axios";
import { properties } from "./../properties";

const PartnerService = {
  async findAll() {
    let config = {
      headers: {
        userId: this.getUserid(),
      },
    };
    console.log(
      "loading sites data from service for user " + config.headers?.userId
    );
    const { data: result } = await axios.get(
      properties.base_url + "/api/partner",
      config
    );
    console.log(result);
    return result;
  },

  async findDetailsById(id) {
    let config = {
      headers: {
        userId: this.getUserid(),
      },
    };
    console.log("loading details data for user " + config.headers?.userId);
    const { data: result } = await axios.get(
      properties.base_url + "/api/partner/" + id,
      config
    );
    console.log("result for id ", id, ": ", result);
    return result;
  },

  getUserid() {
    let user = localStorage.getItem("user");
    if (user === null) {
      return null;
    }
    user = JSON.parse(user);
    return user.id;
  },
};

export default PartnerService;
