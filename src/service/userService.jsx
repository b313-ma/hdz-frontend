import axios from "axios";
import { properties } from "./../properties";

const UserService = {
  async findAll() {
    const { data: result } = await axios.get(
      properties.base_url + "/api/users"
    );
    return result;
  },

  async findById(id) {
    const { data: result } = await axios.get(
      properties.base_url + "/api/users/" + id
    );
    return result;
  },

  isInIndustryOrAdmin() {
    let user = localStorage.getItem("user");
    if (user === null) {
      return false;
    }
    user = JSON.parse(user);
    return (
      user?.role === properties.role_industry ||
      user?.role === properties.role_admin
    );
  },

  isKunde() {
    let user = localStorage.getItem("user");
    if (user === null) {
      return false;
    }
    user = JSON.parse(user);
    return user?.role === properties.role_kunde;
  },
};

export default UserService;
