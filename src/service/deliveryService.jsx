import axios from "axios";
import { properties } from "./../properties";

const DeliveryService = {
  updateDeliveriesTo(deliveries, status) {
    console.log(
      "updating " + deliveries.length + " deliveries to status " + status
    );
    for (let i = 0; i < deliveries.length; i++) {
      console.log("i: " + i);
      let d = deliveries[i];
      d.status = status;
      axios.patch(properties.base_url + "/api/delivery/" + d.id, [
        { op: "replace", path: "/status", value: d.status },
      ]);
    }
  },

  async getDelivery(id) {
    console.log("fetching delivery ", id);
    const { data: result } = await axios.get(
      properties.base_url + "/api/delivery/" + id
    );
    return result;
  },

  async findAll() {
    let config = {
      headers: {
        userId: this.getUserid(),
      },
    };
    console.log("loading all deliveries for user " + config.headers?.userId);
    const { data: result } = await axios.get(
      properties.base_url + "/api/delivery",
      config
    );
    return result;
  },

  getUserid() {
    let user = localStorage.getItem("user");
    if (user === null) {
      return null;
    }
    user = JSON.parse(user);
    return user.id;
  },
};
export default DeliveryService;
