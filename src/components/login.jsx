import { React } from "react";
import { Component } from "react";
import { Dropdown } from "primereact/dropdown";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import UserService from "./../service/userService";
import { withRouter } from "../util/withRouter";

class Login extends Component {
  constructor() {
    super();
    this.state = { allUsers: null, selectedUser: null };
    this.onUserChange = this.onUserChange.bind(this);
    this.login = this.login.bind(this);
  }

  async componentDidMount() {
    const users = await UserService.findAll();
    this.setState({ allUsers: users });
  }

  onUserChange = (e) => {
    this.setState({ selectedUser: e.value });
  };

  login() {
    localStorage.setItem("user", JSON.stringify(this.state.selectedUser));
    this.props.navigate("/");
  }

  render() {
    return (
      <>
        <div className="p-field p-grid">
          <h1>Login</h1>
          <label
            htmlFor="userSelect"
            className="p-col-fixed"
            style={{ width: "300px" }}
          >
            Username
          </label>
          <div className="p-col">
            <Dropdown
              id="userSelect"
              style={{ width: "300px" }}
              value={this.state.selectedUser}
              options={this.state.allUsers}
              onChange={this.onUserChange}
              optionLabel="username"
              placeholder="User auswählen"
            />
          </div>
          <label
            htmlFor="userPassword"
            className="p-col-fixed"
            style={{ width: "300px" }}
          >
            Passwort
          </label>
          <div className="p-col">
            <InputText
              style={{ width: "300px" }}
              disabled={true}
              placeholder="*******"
            />
          </div>
        </div>
        <div className="p-col">
          <Button
            type="button"
            label="Anmelden"
            className="p-button-sm"
            icon="pi pi-check"
            style={{ marginTop: "5px" }}
            disabled={this.state.selectedUser === null}
            onClick={(e) => this.login()}
          />
        </div>
      </>
    );
  }
}

export default withRouter(Login);
