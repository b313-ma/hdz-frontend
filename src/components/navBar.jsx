import React, { Component } from "react";
import { Menubar } from "primereact/menubar";
import { Button } from "primereact/button";
import { withRouter } from "../util/withRouter";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.getUsername = this.getUsername.bind(this);
    this.logout = this.logout.bind(this);
  }

  getUsername() {
    let user = localStorage.getItem("user");
    if (user === null) {
      return null;
    }
    user = JSON.parse(user);
    return user.username;
  }

  logout() {
    localStorage.clear();
    this.props.navigate("/login");
  }

  render() {
    const start = (
      <a className="navbar-brand" href="/">
        Hol' dein Zeug
      </a>
    );
    const end = (
      <>
        <label>{this.getUsername()}</label>{" "}
        <Button
          type="button"
          label="Abmelden"
          className="p-button-rounded p-button-danger p-button-text"
          style={{ height: "25px" }}
          onClick={(e) => this.logout()}
        />
      </>
    );
    return <Menubar start={start} end={this.getUsername() != null && end} />;
  }
}

export default withRouter(NavBar);
