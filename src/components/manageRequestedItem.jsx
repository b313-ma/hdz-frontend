import React, { Component } from "react";
import { withRouter } from "../util/withRouter";
import { Card } from "primereact/card";
import { Button } from "primereact/button";
import { Tag } from "primereact/tag";
import { format } from "date-fns";
import { parseISO } from "date-fns/esm";
import { useParams } from "react-router-dom";
import DeliveryService from "./../service/deliveryService";
import { properties } from "./../properties";

class ManageRequestedItem extends Component {
  constructor(props) {
    super(props);
    this.requestStatusChange = this.requestStatusChange.bind(this);
  }

  state = { delivery: {} };

  async componentDidMount() {
    const delivery = await DeliveryService.getDelivery(this.props.params.id);
    this.setState({ delivery: delivery });
  }

  formatDateTime(value) {
    if (value != null) {
      return format(parseISO(value), "dd.MM.yyyy HH:mm");
    }
    return "";
  }

  calcAndDisplayDaysBetween(from, to) {
    if (from != null && to != null) {
      var fromDate = Date.parse(format(parseISO(from), "yyyy-MM-dd"));
      var toDate = Date.parse(format(parseISO(to), "yyyy-MM-dd"));
      return (
        (new Date(toDate) - new Date(fromDate)) / (1000 * 60 * 60 * 24) +
        " Tage"
      );
    }
    return "";
  }

  requestStatusChange(toStatus) {
    DeliveryService.updateDeliveriesTo([this.state.delivery], toStatus);

    let updDelivery = this.state.delivery;
    updDelivery.status = toStatus;
    this.setState({
      delivery: updDelivery,
    });
  }

  statusDecorator(value) {
    var severityLevel;
    if (value != null) {
      if (value === properties.deliverystate_abholbereit) {
        severityLevel = "success";
      } else if (value === properties.deliverystate_angefordert) {
        severityLevel = "warning";
      } else if (value === properties.deliverystate_angefordert_aufschub) {
        severityLevel = "danger";
      } else {
        severityLevel = "none";
      }
      return <Tag className="mr-2" severity={severityLevel} value={value} />;
    }
    return null;
  }

  render() {
    console.log(this.state.delivery);
    let typeCode = "" + this.state.delivery.cargoSpaceTypeCode;
    let url;
    if (typeCode.includes("Palette")) {
      url =
        "https://treyer.com/files/treyer/img/produkte/europaletten/eur3/treyer-paletten-eur3-aufsetzrahmen-gitterboxen-schwarzwald-deutschland-01.jpg";
    } else if (typeCode.includes("Montage-Stütze")) {
      url =
        "https://cdn02.plentymarkets.com/whubrwlcfsl8/item/images/123817/full/stangenmass-PX-vertikal.jpg";
    }

    const header = <img alt="Card" src={url} style={{ width: "300px" }} />;
    const footer = (
      <span>
        <div className="grid">
          <div className="col-12">
            <Button
              type="button"
              label="Als abholbereit markieren"
              className="p-button-sm p-button-success"
              icon="pi pi-external-link"
              style={{ marginBottom: "10px", marginLeft: "10px" }}
              disabled={
                this.state.delivery?.status ===
                  properties.deliverystate_abgeholt ||
                this.state.delivery?.status ===
                  properties.deliverystate_abholbereit
              }
              onClick={(e) =>
                this.requestStatusChange(properties.deliverystate_abholbereit)
              }
            />
          </div>
          <div className="col-12">
            <Button
              label="1 Woche Aufschub"
              icon="pi pi-times"
              className="p-button-sm p-button-warning"
              style={{ marginBottom: "10px", marginLeft: "10px" }}
              disabled={
                this.state.delivery?.status !==
                properties.deliverystate_angefordert
              }
              onClick={(e) =>
                this.requestStatusChange(
                  properties.deliverystate_angefordert_aufschub
                )
              }
            />
          </div>
          <div className="col-12">
            <Button
              label="Anrufen"
              icon="pi pi-phone"
              className="p-button-sm p-button-secondary"
              style={{ marginBottom: "10px", marginLeft: "10px" }}
              onClick={(e) => window.open("tel:+4367761600977", "_blank")}
            />
          </div>
        </div>
      </span>
    );
    return (
      <>
        <Card
          title={"Anforderung von " + this.state.delivery.fromSiteName}
          subTitle={
            this.state.delivery.cargoSpaceAmount +
            " Stück " +
            this.state.delivery.cargoSpaceTypeCode
          }
          style={{ width: "600px" }}
          footer={footer}
          header={header}
        >
          <div className="grid">
            <div className="col-6">
              <b>Lieferdatum: </b>
            </div>
            <div className="col-6">
              {this.formatDateTime(this.state.delivery.deliveryFulfilled)}
            </div>
            <div className="col-6">
              <b>Angefordert:</b>
            </div>
            <div className="col-6">
              {this.formatDateTime(this.state.delivery.deliveryReturnRequested)}
            </div>
            <div className="col-6">
              <b>Verweildauer:</b>
            </div>
            <div className="col-6">
              {this.calcAndDisplayDaysBetween(
                this.state.delivery.deliveryFulfilled,
                this.state.delivery.deliveryReturnRequested
              )}
            </div>
            <div className="col-6">
              <b>Status:</b>
            </div>
            <div className="col-6">
              {this.statusDecorator(this.state.delivery.status)}
            </div>
          </div>
        </Card>
      </>
    );
  }
}

export default withRouter(ManageRequestedItem);
