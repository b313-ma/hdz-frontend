import React, { Component } from "react";
import PartnerService from "./../service/partnerService";
import DeliveryService from "../service/deliveryService";
import { DataTable } from "primereact/datatable";
import { FilterMatchMode } from "primereact/api";
import { Dropdown } from "primereact/dropdown";
import { Calendar } from "primereact/calendar";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";
import { Tag } from "primereact/tag";
import { format } from "date-fns";
import { parseISO } from "date-fns/esm";
import { properties } from "./../properties";
import UserService from "./../service/userService";

class DeliveriesTable extends Component {
  state = {
    deliveries: null,
    selected: null,
    requestLoading: false,
    deliveryFilter: {
      deliveryFulfilled: {
        value: null,
        matchMode: FilterMatchMode.STARTS_WITH,
      },
      cargoSpaceTypeCode: { value: null, matchMode: FilterMatchMode.CONTAINS },
      cargoSpaceAmount: { value: null, matchMode: FilterMatchMode.IN },
      status: { value: null, matchMode: FilterMatchMode.EQUALS },
      fromSiteName: { value: null, matchMode: FilterMatchMode.CONTAINS },
    },
  };

  constructor(props) {
    super(props);
    this.statusBodyTemplate = this.statusBodyTemplate.bind(this);
    this.statusFilterTemplate = this.statusFilterTemplate.bind(this);
    this.dateBodyTemplate = this.dateBodyTemplate.bind(this);
    this.dateFilterTemplate = this.dateFilterTemplate.bind(this);
    this.isRowSelectable = this.isRowSelectable.bind(this);
    this.getCountOfSelected = this.getCountOfSelected.bind(this);
    this.requestStatusChange = this.requestStatusChange.bind(this);
  }

  async componentDidMount() {
    const deliveries = await DeliveryService.findAll();
    this.setState({ deliveries: deliveries });
  }

  changeSelection(value) {
    this.setState({ selected: value });
  }

  requestStatusChange(fromStatus, toStatus, successmessage) {
    this.setState({ requestLoading: true });
    const affectedDeliveries = this.state.selected.filter(
      (e) => e.status === fromStatus
    );
    DeliveryService.updateDeliveriesTo(affectedDeliveries, toStatus);

    this.setState({
      selected: null,
    });

    this.setState({ requestLoading: false });

    this.toast.show({
      severity: "success",
      summary: successmessage,
      life: 3000,
    });
  }

  statusBodyTemplate(rowData) {
    var severityLevel;
    if (rowData.status === properties.deliverystate_abholbereit) {
      severityLevel = "success";
    } else if (rowData.status === properties.deliverystate_angefordert) {
      severityLevel = "warning";
    } else if (
      rowData.status === properties.deliverystate_angefordert_aufschub
    ) {
      severityLevel = "danger";
    } else {
      severityLevel = "none";
    }
    return (
      <Tag className="mr-2" severity={severityLevel} value={rowData.status} />
    );
  }

  statusFilterTemplate(options) {
    return (
      <Dropdown
        value={options.value}
        options={properties.deliverystatesActive}
        onChange={(e) => options.filterApplyCallback(e.value)}
        placeholder="Status..."
        className="p-column-filter"
        showClear
      />
    );
  }

  formatDate(value) {
    return format(parseISO(value), "dd.MM.yyyy");
  }

  dateBodyTemplate(rowData) {
    return this.formatDate(rowData.deliveryFulfilled);
  }

  dateFilterTemplate(options) {
    return (
      <Calendar
        value={options.value}
        onChange={(e) => options.filterCallback(e.value, options.index)}
        dateFormat="dd.mm.yy"
        placeholder="dd.mm.yy"
        mask="99.99.9999"
      />
    );
  }

  isRowSelectable(event) {
    const data = event.data;
    return data.status !== properties.deliverystate_abgeholt;
  }

  getCountOfSelected(deliveryState) {
    if (this.state.selected != null) {
      return this.state.selected.filter((e) => e.status === deliveryState)
        .length;
    }
    return 0;
  }
  render() {
    const { deliveries, onRowClick } = this.props;
    return (
      <>
        <Toast ref={(el) => (this.toast = el)} />

        {UserService.isInIndustryOrAdmin() && (
          <Button
            type="button"
            label="Anfordern"
            className="p-button-sm p-button-warning"
            icon="pi pi-bell"
            style={{
              marginBottom: "10px",
              marginLeft: "10px",
              marginTop: "10px",
            }}
            badge={this.getCountOfSelected(properties.deliverystate_vor_ort)}
            /*hier länge anders berechnen und in requestPickup dann auch die andere submenge nehmen*/
            disabled={
              this.getCountOfSelected(properties.deliverystate_vor_ort) === 0
            }
            onClick={(e) =>
              this.requestStatusChange(
                properties.deliverystate_vor_ort,
                properties.deliverystate_angefordert,
                "Positionen angefordert"
              )
            }
            loading={this.state.requestLoading}
          />
        )}
        {UserService.isInIndustryOrAdmin() && (
          <Button
            type="button"
            label="Als abgeholt markieren"
            className="p-button-sm"
            icon="pi pi-check"
            style={{
              marginBottom: "10px",
              marginLeft: "10px",
              marginTop: "10px",
            }}
            badge={this.getCountOfSelected(
              properties.deliverystate_abholbereit
            )}
            disabled={
              this.getCountOfSelected(properties.deliverystate_abholbereit) ===
              0
            }
            onClick={(e) =>
              this.requestStatusChange(
                properties.deliverystate_abholbereit,
                properties.deliverystate_abgeholt,
                "Positionen als abgeholt markiert"
              )
            }
            loading={this.state.requestLoading}
          />
        )}

        <Button
          type="button"
          label="Als abholbereit markieren"
          className="p-button-sm p-button-success"
          icon="pi pi-external-link"
          style={{
            marginBottom: "10px",
            marginLeft: "10px",
            marginTop: "10px",
          }}
          badge={
            this.getCountOfSelected(properties.deliverystate_vor_ort) +
            this.getCountOfSelected(properties.deliverystate_angefordert) +
            this.getCountOfSelected(
              properties.deliverystate_angefordert_aufschub
            )
          }
          disabled={
            this.getCountOfSelected(properties.deliverystate_vor_ort) === 0 &&
            this.getCountOfSelected(properties.deliverystate_angefordert) ===
              0 &&
            this.getCountOfSelected(
              properties.deliverystate_angefordert_aufschub
            ) === 0
          }
          onClick={(e) => {
            this.requestStatusChange(
              properties.deliverystate_vor_ort,
              properties.deliverystate_abholbereit,
              "Positionen als abholbereit markiert"
            );
            this.requestStatusChange(
              properties.deliverystate_angefordert,
              properties.deliverystate_abholbereit,
              "Positionen als abholbereit markiert"
            );
            this.requestStatusChange(
              properties.deliverystate_angefordert_aufschub,
              properties.deliverystate_abholbereit,
              "Positionen als abholbereit markiert"
            );
          }}
          loading={this.state.requestLoading}
        />

        <DataTable
          value={this.state.deliveries}
          responsiveLayout="scroll"
          filterDisplay="row"
          removableSort
          sortField="deliveryFulfilled"
          sortOrder={1}
          size="small"
          selection={this.state.selected}
          isDataSelectable={this.isRowSelectable}
          onSelectionChange={(e) => this.changeSelection(e.value)}
          paginator
          rows={20}
          emptyMessage="Keine Positionen an diesem Standort"
        >
          <Column selectionMode="multiple" />
          <Column field="id" header="ID"></Column>
          <Column
            field="toSiteName"
            header="Empfänger"
            filter
            filterPlaceholder="..."
            filterMatchMode="contains"
            sortable
          ></Column>
          <Column
            field="deliveryFulfilled"
            header="Angeliefert am"
            dataType="date"
            body={this.dateBodyTemplate}
            filter
            filterField="deliveryFulfilled"
            filterElement={this.dateFilterTemplate}
            sortable
          ></Column>
          <Column
            field="cargoSpaceTypeCode"
            header="Typ"
            sortable
            filter
            filterPlaceholder="..."
            filterMatchMode="contains"
          ></Column>
          <Column field="cargoSpaceAmount" header="Anzahl" sortable></Column>
          <Column
            field="status"
            header="Status"
            body={this.statusBodyTemplate}
            filter
            filterElement={this.statusFilterTemplate}
            showFilterMenu={false}
            sortable
          />
          <Column
            field="fromSiteName"
            header="Absender"
            filter
            filterPlaceholder="..."
            filterMatchMode="contains"
            sortable
          ></Column>
        </DataTable>
      </>
    );
  }
}

export default DeliveriesTable;
