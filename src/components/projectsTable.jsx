import React, { Component } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";

class ProjectsTable extends Component {
  render() {
    const { sites, onRowClick } = this.props;
    console.log("Table: ", sites);
    return (
      <DataTable
        value={sites}
        responsiveLayout="scroll"
        onRowClick={(e) => onRowClick(e.data.id)}
      >
        <Column field="id" header="ID"></Column>
        <Column field="name" header="Projekt"></Column>
        {/*        <Column field="street" header="Straße"></Column> */}

        <Column field="city" header="Standort"></Column>
      </DataTable>
    );
  }
}

export default ProjectsTable;
