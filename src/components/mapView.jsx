import React, { Component } from "react";
import { MapContainer, Marker, TileLayer } from "react-leaflet";

class MapView extends Component {
  render() {
    let { sites, onMarkerClick } = this.props;
    console.log("mapView rendered, sites: ", sites);

    return (
      //TODO map wird erst beim resizen des windows richtig gesetzt
      //https://stackoverflow.com/questions/55009403/missing-leaflet-map-tiles-when-using-react-leaflet
      <MapContainer center={[48.306938, 14.28583]} zoom={13}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {sites.map((s) => (
          <Marker
            key={s.id}
            position={[s.latitude, s.longitude]}
            eventHandlers={{
              click: () => onMarkerClick(s.id),
            }}
            title={s.name}
            riseOnHover={true}
          />
        ))}
        ;
      </MapContainer>
    );
  }
}

export default MapView;
