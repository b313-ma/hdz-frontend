import React, { Component } from "react";
import ProjectsTable from "./projectsTable";
import { Sidebar } from "primereact/sidebar";
import SiteDetails from "./siteDetails";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import MapView from "./mapView";
import PartnerService from "../service/partnerService";
import DeliveryService from "./../service/deliveryService";
import DeliveriesTable from "./deliveriesTable";

class Main extends Component {
  state = {
    currentDetailId: null,
    sideBarVisible: false,
    constructionSites: [],
  };

  onSiteClick = (siteDetailId) => {
    this.setState({ currentDetailId: siteDetailId, sideBarVisible: true });
  };

  async componentDidMount() {
    const constructionSitesTableDTO = await PartnerService.findAll();
    this.setState({
      constructionSites: constructionSitesTableDTO,
    });
  }

  render() {
    return (
      <>
        <div className="col">
          <Tabs defaultActiveKey="projects">
            <Tab eventKey="projects" title="Projekte">
              <ProjectsTable
                sites={this.state.constructionSites}
                onRowClick={this.onSiteClick}
              />
            </Tab>
            <Tab eventKey="deliveries" title="Lieferungen">
              <DeliveriesTable />
            </Tab>
            <Tab eventKey="map" title="Karte" disabled={true}>
              <MapView
                sites={this.state.constructionSites}
                onMarkerClick={this.onSiteClick}
              />
            </Tab>
          </Tabs>
        </div>
        {this.state.currentDetailId !== null && (
          <Sidebar
            visible={this.state.sideBarVisible}
            position="right"
            modal={true}
            dismissable={true}
            style={{ width: "70%" }}
            onHide={() => this.setState({ sideBarVisible: false })}
          >
            <SiteDetails
              key={this.state.currentDetailId}
              id={this.state.currentDetailId}
            />
          </Sidebar>
        )}
      </>
    );
  }
}

export default Main;
